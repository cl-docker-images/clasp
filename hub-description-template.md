- [Supported Tags](#org5ace8c0)
  - [Simple Tags](#orgc43d9a6)
  - [Shared Tags](#orgf7e6369)
- [Quick Reference](#orgc2b0d63)
- [What is Clasp?](#org5862fc8)
- [How to use this iamge](#org3010311)
  - [Create a `Dockerfile` in your Clasp project](#orgd2660c4)
  - [Run a single Common Lisp script](#org3ee850d)
  - [Developing using SLIME](#orgec0cfc2)
- [What's in the image?](#org300d86b)
- [Image variants](#org4015cbe)
  - [`%%IMAGE%%:<version>`](#org219a6a2)
- [License](#org40c5bee)



<a id="org5ace8c0"></a>

# Supported Tags


<a id="orgc43d9a6"></a>

## Simple Tags

INSERT-SIMPLE-TAGS


<a id="orgf7e6369"></a>

## Shared Tags

INSERT-SHARED-TAGS


<a id="orgc2b0d63"></a>

# Quick Reference

-   **Clasp Home Page:** <https://github.com/clasp-developers/clasp>
-   **Where to file Docker image related issues:** <https://gitlab.common-lisp.net/cl-docker-images/clasp>
-   **Where to file issues for Clasp itself:** <https://github.com/clasp-developers/clasp/issues>
-   **Maintained by:** [CL Docker Images Project](https://common-lisp.net/project/cl-docker-images)
-   **Supported architectures:** `linux/amd64`


<a id="org5862fc8"></a>

# What is Clasp?

From [Clasp's home page](https://github.com/clasp-developers/clasp):

> Clasp is a new Common Lisp implementation that seamlessly interoperates with C++ libraries and programs using LLVM for compilation to native code. This allows Clasp to take advantage of a vast array of preexisting libraries and programs, such as out of the scientific computing ecosystem. Embedding them in a Common Lisp environment allows you to make use of rapid prototyping, incremental development, and other capabilities that make it a powerful language.


<a id="org3010311"></a>

# How to use this iamge


<a id="orgd2660c4"></a>

## Create a `Dockerfile` in your Clasp project

```dockerfile
FROM %%IMAGE%%:latest
COPY . /usr/src/app
WORKDIR /usr/src/app
CMD [ "clasp", "--load", "./your-daemon-or-script.lisp" ]
```

You can then build and run the Docker image:

```console
$ docker build -t my-clasp-app
$ docker run -it --rm --name my-running-app my-clasp-app
```


<a id="org3ee850d"></a>

## Run a single Common Lisp script

For many simple, single file projects, you may find it inconvenient to write a complete \`Dockerfile\`. In such cases, you can run a Lisp script by using the Clasp Docker image directly:

```console
$ docker run -it --rm --name my-running-script -v "$PWD":/usr/src/app -w /usr/src/app %%IMAGE%%:latest clasp --load ./your-daemon-or-script.lisp
```


<a id="orgec0cfc2"></a>

## Developing using SLIME

[SLIME](https://common-lisp.net/project/slime/) provides a convenient and fun environment for hacking on Common Lisp. To develop using SLIME, first start the Swank server in a container:

```console
$ docker run -it --rm --name clasp-slime -p 127.0.0.1:4005:4005 -v /path/to/slime:/usr/src/slime -v "$PWD":/usr/src/app -w /usr/src/app %%IMAGE%%:latest clasp --load /usr/src/slime/swank-loader.lisp --eval '(swank-loader:init)' --eval '(swank:create-server :dont-close t :interface "0.0.0.0")'
```

Then, in an Emacs instance with slime loaded, type:

```emacs
M-x slime-connect RET RET RET
```


<a id="org300d86b"></a>

# What's in the image?

This image contains Clasp binaries built from the latest source releases from the Clasp devs.


<a id="org4015cbe"></a>

# Image variants

This image comes in several variants, each designed for a specific use case.


<a id="org219a6a2"></a>

## `%%IMAGE%%:<version>`

This is the defacto image. If you are unsure about what your needs are, you probably want to use this one. It is designed to be used both as a throw away container (mount your source code and start the container to start your app), as well as the base to build other images off of. Additionally, these images contain the Clasp source code (at `/usr/local/src/clasp`) to help facilitate interactive development and exploration (a hallmark of Common Lisp!).

Some of these tags may have names like buster or stretch in them. These are the suite code names for releases of Debian and indicate which release the image is based on. If your image needs to install any additional packages beyond what comes with the image, you'll likely want to specify one of these explicitly to minimize breakage when there are new releases of Debian.

These images are built off the buildpack-deps image. It, by design, has a large number of extremely common Debian packages.

These images contain the Quicklisp installer, located at `/usr/local/share/common-lisp/source/quicklisp/quicklisp.lisp`. Additionally, there is a script at `/usr/local/bin/install-quicklisp` that will use the bundled installer to install Quicklisp. You can configure the Quicklisp install with the following environment variables:

-   **`QUICKLISP_DIST_VERSION`:** The dist version to use. Of the form yyyy-mm-dd. `latest` means to install the latest version (the default).
-   **`QUICKLISP_CLIENT_VERSION`:** The client version to use. Of the form yyyy-mm-dd. `latest` means to install the latest version (the default).
-   **`QUICKLISP_ADD_TO_INIT_FILE`:** If set to `true`, `(ql:add-to-init-file)` is used to add code to the implementation's user init file to load Quicklisp on startup. Not set by default.

Additionally, these images contain cl-launch to provide a uniform interface to running a Lisp implementation without caring exactly which implementation is being used (for instance to have uniform CI scripts).


<a id="org40c5bee"></a>

# License

Clasp is licensed under the [GNU LGPL v2.1+](https://opensource.org/licenses/LGPL-2.1).

The Dockerfiles used to build the images are licensed under BSD-2-Clause.

As with all Docker images, these likely also contain other software which may be under other licenses (such as Bash, etc from the base distribution, along with any direct or indirect dependencies of the primary software being contained).

As for any pre-built image usage, it is the image user's responsibility to ensure that any use of this image complies with any relevant licenses for all software contained within.
